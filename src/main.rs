use std::env;

use futures::StreamExt;
use log::{info, warn};
use std::error::Error;
use telegram_bot::{Api, CanReplySendMessage, MessageKind, UpdateKind, UserId};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    env_logger::init();

    let bot: Api = {
        let token = env::var("BOT_TOKEN").expect("BOT_TOKEN not set");
        Api::new(token)
    };

    let owner_id = UserId::new(
        env::var("OWNER")
            .expect("OWNER chat id not set")
            .parse::<i64>()?,
    );

    let mut stream = bot.stream();
    while let Some(update) = stream.next().await {
        let update = update?;
        if let UpdateKind::Message(message) = update.kind {
            if let MessageKind::Text { ref data, .. } = message.kind {
                if message.from.id == owner_id {
                    if data == "/get_ip" {
                        bot.send(message.text_reply(format!(
                            "Ciao, {}! Il mio indirizzo IP è {}",
                            &message.from.first_name,
                            get_ip_address().await?
                        )))
                        .await?;
                    } else {
                        warn!("Unsupported command: {}", data);
                    }
                } else {
                    warn!("Unexpected user: {:?}", message.from);
                }
            }
        }
    }
    Ok(())
}

async fn get_ip_address() -> Result<String, Box<dyn Error + Send + Sync>> {
    static IP_URL: &str = "http://bot.whatismyipaddress.com";
    let ip_addr = reqwest::get(IP_URL).await?.text().await?;

    info!("IP address fetched: {}", ip_addr);

    Ok(ip_addr)
}

#[tokio::test]
async fn run_ip_get() {
    get_ip_address().await.expect("IP request failed");
}
